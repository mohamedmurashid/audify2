package com.audify.app.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.audify.app.R
import com.audify.app.dp.Songs
import com.audify.app.databinding.SongsListItemBinding

class SongListAdapter(val clickListener: ItemClickListener) : ListAdapter<Songs, SongListAdapter.ViewHolder>(SongDiffCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position)!!, clickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: SongsListItemBinding) : RecyclerView.ViewHolder(binding.root){

        fun bind(item: Songs, clickListener: ItemClickListener) {
            binding.songList = item
            binding.position = layoutPosition;
            binding.clickListener = clickListener
            /* binding.ivFavourite.setImageResource(when (item.favorite) {
                 true -> R.drawable.ic_favorite_selected
                 else -> R.drawable.ic_favorite_border
             })*/
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = SongsListItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

class SongDiffCallback : DiffUtil.ItemCallback<Songs>() {
    override fun areItemsTheSame(oldItem: Songs, newItem: Songs): Boolean {
        return oldItem.songId == newItem.songId
    }

    override fun areContentsTheSame(oldItem: Songs, newItem: Songs): Boolean {
        return oldItem == newItem
    }
}

class ItemClickListener(val itemClickListener:(song :Songs) -> Unit, val favouriteClickListener: (song : Songs, position : Int) -> Unit ) {
    fun onItemClick (song : Songs) = itemClickListener(song)
    fun onFavourite (song : Songs, position : Int) = favouriteClickListener(song, position)
}