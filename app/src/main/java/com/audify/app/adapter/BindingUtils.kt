
package com.audify.app.adapter

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.audify.app.dp.Songs
import com.audify.app.R


@BindingAdapter("songName")
fun TextView.setSongName(item: Songs?) {
    item?.let {
        text = item.songName
    }
}


@BindingAdapter("songAlbumName")
fun TextView.setAlbumName(item: Songs?) {
    item?.let {
        text = item.songAlbumName
    }
}


@BindingAdapter("favourite")
fun ImageView.setFavorite(item: Songs?) {
    item?.let {
        setImageResource(when (item.favorite) {
            true -> R.drawable.ic_favorite_selected
            else -> R.drawable.ic_favorite_border
        })
    }
}