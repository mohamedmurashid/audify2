
package com.audify.app.ui

import android.Manifest
import android.app.ActivityManager
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.audify.app.adapter.ItemClickListener
import com.audify.app.adapter.SongListAdapter
import com.audify.app.databinding.ActivityMainBinding
import com.audify.app.dp.AppDatabase
import com.audify.app.dp.Songs
import com.audify.app.service.AudioService
import com.audify.app.viewmodel.MainViewModel
import com.audify.app.viewmodel.MainViewModelFactory
import com.google.android.exoplayer2.SimpleExoPlayer


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private var audioService: AudioService? = null
    private var mBound = false

    private val mConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            val binder: AudioService.LocalBinder = iBinder as AudioService.LocalBinder
            audioService = binder.service
            mBound = true
            initializePlayer()
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            mBound = false
        }
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) !==
                PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(this@MainActivity,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
            } else {
                ActivityCompat.requestPermissions(this@MainActivity,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
            }
        } else {
            init()
        }


    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1 && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            init()
        }
    }

    private fun init() {
        val application = requireNotNull(this).application

        val dataSource = AppDatabase.getInstance(application).songsDao
        val viewModelFactory = MainViewModelFactory(dataSource, application)
        val mainViewModel = ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)

        binding.mainViewModel = mainViewModel

       val adapter = SongListAdapter(ItemClickListener({ song ->
           playSong(song)
       }, { song, position ->
           binding.mainViewModel?.insetDeleteSong(song, position)
       }))


        binding.sleepList.adapter = adapter

        binding.mainViewModel?.songList?.observe(this, Observer {
            it?.let {
                adapter.submitList(it)
            }
        })
        binding.mainViewModel?.isPositionUpdated?.observe(this, Observer {
            it?.let { it1 -> adapter.notifyItemChanged(it1) }
        })

        binding.setLifecycleOwner(this)

        binding.ivClose.setOnClickListener({ stopAudioService() })
    }

    private fun stopAudioService() {
        val intent = Intent(this, AudioService::class.java)
        intent.action = "STOP"
        binding.videoView.visibility = View.INVISIBLE;
        binding.tvTitle.visibility = View.INVISIBLE;
        binding.ivClose.visibility = View.INVISIBLE;
        startService(intent)
    }

    private fun playSong(it: Songs) {
        val intent = Intent(this, AudioService::class.java)
        intent.action = "START"
        val bundle = Bundle();
        bundle.putParcelable("SONG", it)
        intent.putExtras(bundle)
        startForegroundService(intent)
        binding.videoView.visibility = View.VISIBLE;
        binding.videoView.controllerShowTimeoutMs = 1000000;
        binding.tvTitle.visibility = View.VISIBLE;
        binding.ivClose.visibility = View.VISIBLE;
        binding.tvTitle.setText(it.songName)
    }

    private fun initializePlayer() {
        if (mBound) {
            val player: SimpleExoPlayer? = audioService?.getPlayerInstance()
            player.also { exoPlayer ->
                binding.videoView.player = exoPlayer
                if(player?.isPlaying!!) {
                    binding.videoView.visibility = View.VISIBLE;
                    binding.videoView.controllerShowTimeoutMs = 1000000;
                    binding.videoView.showController();
                    binding.tvTitle.visibility = View.VISIBLE;
                    binding.ivClose.visibility = View.VISIBLE;
                    binding.tvTitle.setText(audioService?.getSongName())
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        Intent(this, AudioService::class.java).also { intent ->
            bindService(intent, mConnection, BIND_AUTO_CREATE)
        }
    }

    override fun onStop() {
        unbindService(mConnection)
        mBound = false
        super.onStop()
    }
}
