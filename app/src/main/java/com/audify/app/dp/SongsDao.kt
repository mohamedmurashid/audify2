
package com.audify.app.dp

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface SongsDao {

    @Query("SELECT * FROM " + DatabaseConstant.SONGS_TABLE_NAME)
    suspend fun getSongs(): List<Songs>

    @Insert
    suspend fun insert(song: Songs)

    @Query("DELETE FROM " + DatabaseConstant.SONGS_TABLE_NAME.toString() + " WHERE " + DatabaseConstant.SONGS_NAME.toString() + "=:song")
    suspend fun deleteSong(song: String)
}
