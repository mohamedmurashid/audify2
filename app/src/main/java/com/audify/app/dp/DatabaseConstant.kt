package com.audify.app.dp

interface DatabaseConstant {
    companion object {
        const val DATABASE_NAME = "AudifyDB"
        const val DATABASE_VERSION = 4
        const val SONGS_TABLE_NAME = "Songs"
        const val SONGS_NAME = "SongsName"
        const val SONGS_ALBUM_NAME = "SongsAlbumName"
        const val FAVORITE = "Favorite"
        const val URI = "Uri"
    }
}