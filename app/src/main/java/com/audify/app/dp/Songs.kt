
package com.audify.app.dp

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = DatabaseConstant.SONGS_TABLE_NAME)
data class Songs(
        @PrimaryKey(autoGenerate = true)
        var songId: Long = 0L,

        @ColumnInfo(name = DatabaseConstant.SONGS_NAME)
        var songName: String = "" ,

        @ColumnInfo(name = DatabaseConstant.FAVORITE)
        var favorite: Boolean = false ,

        @ColumnInfo(name = DatabaseConstant.URI)
        var uri: String = "" ,

        @ColumnInfo(name = DatabaseConstant.SONGS_ALBUM_NAME)
        var songAlbumName: String = "")  : Parcelable {
        constructor(parcel: Parcel) : this(
                parcel.readLong(),
                parcel.readString()!!,
                parcel.readByte() != 0.toByte(),
                parcel.readString()!!,
                parcel.readString()!!) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeLong(songId)
                parcel.writeString(songName)
                parcel.writeByte(if (favorite) 1 else 0)
                parcel.writeString(uri)
                parcel.writeString(songAlbumName)
        }

        override fun describeContents(): Int {
                return 0
        }

        companion object CREATOR : Parcelable.Creator<Songs> {
                override fun createFromParcel(parcel: Parcel): Songs {
                        return Songs(parcel)
                }

                override fun newArray(size: Int): Array<Songs?> {
                        return arrayOfNulls(size)
                }
        }
}
