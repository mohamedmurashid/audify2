
package com.audify.app.viewmodel

import android.app.Application
import android.database.MergeCursor
import android.provider.MediaStore
import android.util.Log
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import com.audify.app.dp.Songs
import com.audify.app.dp.SongsDao
import kotlinx.coroutines.*
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class MainViewModel(
        val database: SongsDao,
        val application: Application) : ViewModel() {

    val songList: MutableLiveData<ArrayList<Songs>> = MutableLiveData();
    private var _positionUpdated = MutableLiveData<Int?>()

    val isPositionUpdated: LiveData<Int?>
        get() = _positionUpdated

    fun insetDeleteSong(song : Songs, position : Int) {
        viewModelScope.launch {
            if (!song.favorite) {
                val newSong = Songs()
                newSong.songName = song.songName
                database.insert(newSong)
            } else {
               database.deleteSong(song.songName)
            }
            song.favorite = !song.favorite
            songList.value?.set(position,song)
            _positionUpdated.value = position;
        }
    }

    init {
        GlobalScope.launch(Dispatchers.Main) {
            val favouriteSongs = withContext(Dispatchers.IO) { database.getSongs() }
            val songs : ArrayList<Songs> =  withContext(Dispatchers.IO) { getSongFromDevice(favouriteSongs) }
            songList.postValue(songs);
        }
    }

    private fun getSongFromDevice(favouriteSongs: List<Songs>): ArrayList<Songs> {
        val songList = ArrayList<Songs>()
        try {
            val columns = arrayOf(MediaStore.Images.Media.DATA,
                    MediaStore.Images.Media.DATE_ADDED,
                    MediaStore.Images.Media.BUCKET_ID,
                    MediaStore.Images.Media.BUCKET_DISPLAY_NAME)
            val cursor = MergeCursor(arrayOf(application.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, columns, null, null, null)))
            cursor.moveToFirst()
            songList.clear()
            while (!cursor.isAfterLast) {
                var path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA))
                val lastPoint = path.lastIndexOf(".")
                path = path.substring(0, lastPoint) + path.substring(lastPoint).toLowerCase()
                val file = File(path)
                val songs = Songs()
                songs.songName = file.name.substring(0, file.name.length - 3)
                songs.songAlbumName = "Alubum : " + file.name
                songs.uri = file.toURI().toString()
                for(item in favouriteSongs) {
                    if(item.songName.equals(songs.songName)) {
                        songs.favorite = true;
                    }
                }
                songList.add(songs)
                cursor.moveToNext()
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
        Log.d("Murashid", "getSongFromDevice: "+songList.size)
        return songList;
    }
}