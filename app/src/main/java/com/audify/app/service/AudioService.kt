package com.audify.app.service

import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Binder
import android.os.Bundle
import android.os.IBinder
import com.audify.app.R
import com.audify.app.dp.Songs
import com.audify.app.ui.MainActivity
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.PlayerNotificationManager
import com.google.android.exoplayer2.ui.PlayerNotificationManager.*
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.android.exoplayer2.video.spherical.CameraMotionListener

class AudioService : Service() {

    private var player: SimpleExoPlayer? = null
    private lateinit var playerNotificationManager: PlayerNotificationManager

    private var song : Songs? =null

    private val binder = LocalBinder()

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    inner class LocalBinder : Binder() {
        val service: AudioService
            get() = this@AudioService
    }

    override fun onCreate() {
        super.onCreate()
        val trackSelector = DefaultTrackSelector(this).apply {
            setParameters(buildUponParameters().setMaxVideoSizeSd())
        }
        player = SimpleExoPlayer.Builder(this)
                .setTrackSelector(trackSelector)
                .build()
        player?.playWhenReady = true
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if(intent?.action.equals("START")) {
            val bundle = intent?.extras
            if (bundle != null) {
                song = bundle.getParcelable("SONG")
            }
            if(player == null) {
                val trackSelector = DefaultTrackSelector(this).apply {
                    setParameters(buildUponParameters().setMaxVideoSizeSd())
                }
                player = SimpleExoPlayer.Builder(this)
                        .setTrackSelector(trackSelector)
                        .build()
                player?.playWhenReady = true
            }
            addNotificationToPlayer()
            return START_REDELIVER_INTENT
        } else if (intent?.action.equals("STOP")) {
            player?.stop()
            stopForeground(true)
        }
        return START_NOT_STICKY
    }

    private fun addNotificationToPlayer() {
        playerNotificationManager = createWithNotificationChannel(this, "Murashid",
                R.string.app_name,
                2,
                object : MediaDescriptionAdapter {
                    override fun getCurrentContentTitle(player: Player): String {
                        return song?.songName!!
                    }

                    override fun createCurrentContentIntent(player: Player): PendingIntent? {
                        val intent = Intent(this@AudioService, MainActivity::class.java)
                        return PendingIntent.getActivity(this@AudioService, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
                    }

                    override fun getCurrentContentText(player: Player): String? {
                        return song?.songAlbumName!!
                    }

                    override fun getCurrentLargeIcon(player: Player, callback: BitmapCallback): Bitmap? {
                        return null
                    }
                }
        )
        playerNotificationManager.setNotificationListener(object : NotificationListener {
            override fun onNotificationStarted(notificationId: Int, notification: Notification) {
                startForeground(notificationId, notification)
            }

            override fun onNotificationCancelled(notificationId: Int) {
                stopSelf()
            }

        })
        playerNotificationManager.setUseNavigationActions(false);
        // omit fast forward action by setting the increment to zero
        playerNotificationManager.setFastForwardIncrementMs(0);
        // omit rewind action by setting the increment to zero
        playerNotificationManager.setRewindIncrementMs(0);

        playerNotificationManager.setSmallIcon(R.drawable.ic_launcher)

        playerNotificationManager.setColor(getColor(R.color.exo_black_opacity_30))

        //assign the player to it
        playerNotificationManager.setPlayer(player)
        val mediaSource = extractMediaSourceFromUri(Uri.parse(song?.uri))
        if(player?.isPlaying == true) {
            player?.stop();
        }
        player?.prepare(mediaSource)
    }

    override fun onTaskRemoved(rootIntent: Intent) {
       // stopService()
        //super.onTaskRemoved(rootIntent)
    }


    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
    }

    override fun onDestroy() {
        if (::playerNotificationManager.isInitialized) {
            playerNotificationManager.setPlayer(null)
        }
        player?.release()
        super.onDestroy()
    }

    private fun extractMediaSourceFromUri(uri: Uri): MediaSource {
        val userAgent = Util.getUserAgent(this, "Exo")
        return ExtractorMediaSource.Factory(DefaultDataSourceFactory(this, userAgent))
                .setExtractorsFactory(DefaultExtractorsFactory()).createMediaSource(uri)
    }

    fun getPlayerInstance(): SimpleExoPlayer? {
        return player;
    }

    fun getSongName(): String? {
        return song?.songName;
    }


}